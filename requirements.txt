# Documentation static site generator & deployment tool
mkdocs==1.4.0

# Add your custom theme if not inside a theme_dir
# (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)

mkdocs-material
mkdocs-git-revision-date-plugin
mkdocs-git-revision-date-localized-plugin


#mkdocs-awesome-pages-plugin
