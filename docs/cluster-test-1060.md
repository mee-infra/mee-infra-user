# Mise en place d'un cluster de test

## Topologie

- Master :  pc-elec-385
- Nodes : pc-elec-3[85-96]
- DB : pc-elec-386


## Soumission de jobs avec sbatch

# Sans allocation de GPU
Script `sbatch` :

``` bash
#!/bin/bash

#SBATCH --job-name=test-gpu_%j
#SBATCH --output=Output_%j.txt

srun monprogram.o
```

# Avec allocation de GPU
Script `sbatch` :

``` bash
#!/bin/bash

#SBATCH --job-name=test-gpu_%j
#SBATCH --output=Output_%j.txt

#SBATCH --gres=gpu:gtx1060:1

srun monprogram.o
```

## Directives `#SBATCH`

| Directive    | Description                                                |
| -------------- | ------------------------------------------------------------ |
| `--job-name` | Nom souhaité pour le job (facilement identifiable ensuite) |
| `--output`   | Nom souhaité pour le fichier de sortie de Slurm            |
| `--gres=`    | Demande d'allocation de Generic RESources                  |

Il en existe beaucoup d'autres : [https://slurm.schedmd.com/sbatch.html](https://slurm.schedmd.com/sbatch.html)
